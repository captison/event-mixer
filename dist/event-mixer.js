/*! event-mixer v0.0.5 @Tue, 18 Apr 2017 16:34:57 GMT */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["eventMixer"] = factory();
	else
		root["eventMixer"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

// helper methods
var cap = function cap(s) {
    return s.slice(0, 1).toUpperCase() + s.slice(1);
};
var map = function map(v, f) {
    return [].concat(v).map(f);
};

exports.mixer = function () {
    var _target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    var _emitter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var events = {},
        targetFns = ['on', 'off'];
    // create new event handler
    var _create = function _create(name, data) {
        var cname = cap(name);
        // add on/off functions to target for this event
        targetFns.forEach(function (n) {
            return _target[n + cname] = function (f) {
                return this[n](name, f);
            };
        });
        // add fire function to emitter for this event
        _emitter['fire' + cname] = function (d) {
            return this.fire(name, d);
        };
        // add event handler
        return events[name] = exports.handler(_target, name, data);
    };
    // clear named event handler
    var _destroy = function _destroy(name) {
        var cname = cap(name),
            handler = events[name];
        // clear and remove event handler
        events[name].clear();delete events[name];
        // remove on/off functions from target for this event
        targetFns.forEach(function (n) {
            return delete _target[n + cname];
        });
        // remove fire function from emitter for this event
        delete _emitter['fire' + cname];

        return handler;
    };

    var methods = {
        // create/register a new event for target
        create: function create(n, d) {
            return _create(n, d);
        },

        // destroy/unregister event for target
        destroy: function destroy(n) {
            return _destroy(n);
        },

        // return the emitter for this event manager
        emitter: function emitter() {
            return _emitter;
        },

        // return the named event
        event: function event(n) {
            return events[n];
        },

        // fire the named event
        fire: function fire(n, d) {
            return events[n].fire(d);
        },

        // is the named event registered?
        has: function has(n) {
            return events[n] ? true : false;
        },

        // return the names of all registered events
        names: function names() {
            return Object.keys(events);
        },

        // remove listener(s) for named event(s)
        off: function off(n, f) {
            map(n, function (x) {
                return events[x].remove(f);
            });return this;
        },

        // add listener(s) for named event(s)
        on: function on(n, f) {
            map(n, function (x) {
                return events[x].append(f);
            });return this;
        },

        // return the target of this event manager
        target: function target() {
            return _target;
        }
    };
    // add on/off methods to target
    targetFns.forEach(function (n) {
        return _target[n] = methods[n];
    });
    // add fire method to emitter
    _emitter.fire = methods.fire;

    return methods;
};

exports.handler = function (target, name, hdata) {
    var listeners = [];

    var _fire = function _fire(idata) {
        // should the result of this event (if any) be allowed?
        var allow = true;
        // event data - invocation data overwrites handler data
        var data = Object.assign({}, hdata, idata);

        for (var i = 0; i < listeners.length; i++) {
            // call listener with an 'event object'
            var rtn = listeners[i]({ target: target, name: name, data: data }) || {};
            // listener can indicate loss of interest in this event
            if (rtn.remove) {
                listeners.splice(i, 1);i--;
            }
            // listener can indicate a desire to prevent whatever comes next
            if (rtn.prevent) allow = false;
            // listener can end notifications
            if (rtn.stop) break;
        }

        return allow;
    };

    var methods = {
        // append listener(s)
        add: function add(f) {
            return this.append(f);
        },

        // add listener(s) to end of list
        append: function append(f) {
            map(f, this.push);return this;
        },

        // returns the listener at the given index
        at: function at(i) {
            return i >= 0 && i < listeners.length ? listeners[i] : null;
        },

        // remove all listeners
        clear: function clear() {
            listeners = [];return this;
        },

        // return the number of listeners for this event
        count: function count() {
            return listeners.length;
        },

        // removes and returns the listener at index
        cut: function cut(i) {
            return i >= 0 && i < listeners.length ? listeners.splice(i, 1)[0] : null;
        },

        // filter/change the listener list
        each: function each(m) {
            var _this = this;

            [].concat(_toConsumableArray(listeners)).reduce(function (a, l, i) {
                return _this.push(m(l, i));
            }, listeners = []);return this;
        },

        // is the given function a listener on this event?
        has: function has(f) {
            return listeners.indexOf(f) >= 0;
        },

        // return the index of a given listener
        indexOf: function indexOf(f, s) {
            return listeners.indexOf(f, s);
        },

        // trigger this event (with data)
        fire: function fire(d) {
            return _fire(d);
        },

        // add listener(s) to beginning of list
        prepend: function prepend(f) {
            [].concat(f).reverse().map(this.unshift);return this;
        },

        // add last listener
        push: function push(f) {
            return typeof f === 'function' && listeners.push(f);
        },

        // remove listener(s)
        remove: function remove(f) {
            var _this2 = this;

            map(f, function (m) {
                return _this2.cut(listeners.indexOf(m));
            });return this;
        },

        // add first listener
        unshift: function unshift(f) {
            return typeof f === 'function' && listeners.unshift(f);
        }
    };

    return methods;
};

/***/ })
/******/ ]);
});