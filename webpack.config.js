var path = require('path');
var webpack = require('webpack');
var packson = require('./package.json');


var moduleDir = __dirname;

module.exports = function(env)
{
    var config = {};

    config.entry =
    {
        [packson.name]: path.join(moduleDir, 'index.js')
    };

    config.output =
    {
        path: path.join(moduleDir, 'dist'),
        libraryTarget: 'umd',
        library: 'eventMixer',
        // filename: '[name].js'
    };

    config.resolve = { extensions: ['.js'] };

    var es6Loader =
    {
        test : /\.js$/,
        include: [moduleDir],
        loader: 'babel-loader',
        options: { presets: ['env'] }
    };

    config.module = { rules: [ es6Loader ] };

    var bannerPluginOptions =
    {
        // the banner as string, it will be wrapped in a comment
        banner: packson.name + ' v' + packson.version + ' @' + new Date().toUTCString(),
        // if true, banner will not be wrapped in a comment
        raw: false,
        // if true, the banner will only be added to the entry chunks
        entryOnly: true
    };

    config.plugins =
    [
        new webpack.BannerPlugin(bannerPluginOptions),
    ];

    return config;
};
