## EventMixer Change Log

### In Development (TODOs)


### Releases

#### 0.0.5

- added .filter() method to handler for mutating listener list
- .indexOf() now accepts a start index

#### 0.0.4

- mixer now accepts an optional emitter for mixing-in .fire() methods
- mixer target parameter is now optional

#### 0.0.3

- code updates for grading (codacy)
- readme badges added

#### 0.0.2

- multiple listeners can now be added/removed via array parameters
- fixed bug preventing 'stop' and 'prevent' flags in listener return value from being used together
- tests updated

#### 0.0.1

- first version of **EventMixer**!
