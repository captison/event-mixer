var Jasmine = require('jasmine');
var JasmineConsoleReporter = require('jasmine-console-reporter');


var jasmine = new Jasmine();

var config =
{
    spec_dir: 'test',
    spec_files: ['**/*.spec.js'],
    helpers:
    [
        '../node_modules/jasmine-expect/index.js',
    ],
    stopSpecOnExpectationFailure: false,
    random: false
};

jasmine.loadConfig(config);

var reporterConfig =
{
    colors: 1,           // (0|false)|(1|true)|2
    cleanStack: 1,       // (0|false)|(1|true)|2|3
    verbosity: 3,        // (0|false)|1|2|(3|true)|4
    listStyle: 'indent', // 'flat'|'indent'
    activity: false
};

jasmine.addReporter(new JasmineConsoleReporter(reporterConfig));

module.exports = jasmine;
